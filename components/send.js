import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Alert,
    ScrollView
} from 'react-native';

const giftCollection = [
  {
    title: 'AN ICE CREAM',
    imgResource: require('./img/gifts/icecream.png'),
    img: 'icecream.png'
  },
  {
    title: 'A BEER',
    imgResource: require('./img/gifts/beer.png'),
    img: 'beer.png'
  },
  {
    title: 'FIKA',
    imgResource: require('./img/gifts/fika.png'),
    img: 'fika.png'
  },
  {
    title: 'A LAP DANCE FROM PIERRE',
    imgResource: require('./img/gifts/lapdance.png'),
    img: 'lapdance.png'
  },
  {
    title: 'A MOVIE TICKET',
    imgResource: require('./img/gifts/movieticket.png'),
    img: 'movieticket.png'
  },
  {
    title: 'A HUG',
    imgResource: require('./img/gifts/hug.png'),
    img: 'hug.png'
  },
  {
    title: 'LUNCH',
    imgResource: require('./img/gifts/lunch.png'),
    img: 'lunch.png'
  },
  {
    title: 'A SNICKERS',
    imgResource: require('./img/gifts/snickers.png'),
    img: 'snickers.png'
  },
  {
    title: 'A COCKTAIL',
    imgResource: require('./img/gifts/cocktail.png'),
    img: 'cocktail.png'
  },
  {
    title: 'A BOX OF STRAWBERRIES',
    imgResource: require('./img/gifts/strawberries.png'),
    img: 'strawberries.png'
  },
  {
    title: 'PRIO LANE FOR FRIDAY SWEETS',
    imgResource: require('./img/gifts/priolane.png'),
    img: 'priolane.png'
  }
];

export default class SendForm extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
          user_suggestions: [],
          user_selected: '',
          gifts: giftCollection,
          gift_selected: 0,
          message: "Thanks a lot for your help.\r\n You are my hero!"
        };
    }

    render() {
        return (
          <Image source={require('./img/bg_fuzzy.png')} style={styles.background}>
              <Text style={styles.header}>I OWE YOU</Text>
              <Text style={styles.giftTitle}>{this.selectedGift.bind(this)().title}</Text>
              <View style={styles.giftField}>
                <TouchableOpacity onPress={this.prevGift.bind(this)} style={styles.giftArrowPlaceholder}>
                  <Image source={require('./img/arrow_left.png')}
                  style={styles.giftSwapArrow}
                  resizeMode={'contain'} />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image source={this.selectedGift.bind(this)().imgResource}
                    style={styles.giftImage} />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.nextGift.bind(this)} style={styles.giftArrowPlaceholder}>
                  <Image source={require('./img/arrow_right.png')} style={styles.giftSwapArrow} />
                </TouchableOpacity>
              </View>
              <TextInput style={styles.txtInput}
                onChangeText={(recipient) => this.setState({user_selected: recipient})}
                value={this.state.user_selected}
                keyboardType={'email-address'}
                placeholder={'To:'}
              />
              <TextInput style={styles.txtInput}
                onChangeText={(message) => this.setState({message})}
                value={this.state.message}
                maxLength={60}
                multiline={true}
                numberOfLines={2}
                placeholder={'Message:'}
              />
              <TouchableOpacity onPress={this.submitTicket.bind(this)} style={styles.button}>
                <Text style={styles.buttonTxt}>SEND</Text>
              </TouchableOpacity>
          </Image>
        );
    }

    selectedGift() {
      return this.state.gifts[this.state.gift_selected];
    }

    nextGift() {
      const nextIndex = ((this.state.gift_selected+1)%giftCollection.length);
      this.setState({gift_selected: nextIndex});
    }

    prevGift() {
      // There is apparently a bug in JS calculating mudulus on neg numbers
      const len = giftCollection.length;
      const prevIndex = ((len+this.state.gift_selected-1)%giftCollection.length);
      console.log(prevIndex);
      this.setState({gift_selected: prevIndex});
    }

    submitTicket() {
      const from = this.context.getUser().email;
      const gift = encodeURI(JSON.stringify(this.selectedGift.bind(this)()));
      const message = encodeURI(this.state.message);
      const endpointUrl = this.context.apiBase + '/tickets?from='+from+'&to='+this.state.user_selected+'&gift='+gift+'&note='+message;
      console.log(endpointUrl);
      fetch(endpointUrl, {method: 'POST'})
      .then((response) => response.json())
      .then((responseData) => {
        console.log(responseData);
        if (responseData.id) {
          Alert.alert(
            'IOU Ticket sent', '',
            [{text: 'OK', onPress: this.clear.bind(this)}]
          )
        } else if (responseData.message) {
          Alert.alert(
            'Error', responseData.message,
            [{text: 'OK'}]
          )
        } else {
          Alert.alert(
            'Unknown Error', '',
            [{text: 'OK'}]
          )
        }

      })
      .done(
      );
    }

    clear() {
      this.setState({
        user_suggestions: [],
        user_selected: '',
        gift_selected: 0,
        message: ''
      });
    }
}

SendForm.contextTypes = {
  apiBase: React.PropTypes.string,
  getUser: React.PropTypes.any // Should of course be function, but can't get the validator to accept.
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'flex-start',
    width: null,
    height: null
  },
  header: {
    fontSize: 25,
    color: '#FFFFFF',
    textAlign: 'center',
    width: Dimensions.get('window').width,
    marginTop: 10
  },
  giftTitle: {
    fontSize: 20,
    color: '#FFFFFF',
    textAlign: 'center',
    width: Dimensions.get('window').width,
  },
  giftField: {
    flexDirection: 'row',
    position: 'relative',
    marginTop: 20
  },
  giftImage: {
    height: 200,
    width: Dimensions.get('window').width/2,
    alignSelf: 'flex-start'
  },
  giftSwapArrow: {
    height: 20
  },
  giftArrowPlaceholder: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width/4
  },
  txt: {
    fontSize: 20,
    color: '#FFFFFF',
    textAlign: 'center',
    margin: 10,
  },
  txtInput: {
    color: '#000000',
    fontSize: 20,
    borderColor: '#FFFFFF',
    borderWidth: 1,
    marginHorizontal: 30,
    marginVertical: 5,
    backgroundColor: '#FFFFFF',
    opacity: 0.5
  },
  txtArea: {
  },
  button: {
    backgroundColor: '#FFEBCC',
    position: 'absolute',
    bottom: 0,
    height: 50,
    width: Dimensions.get('window').width,
    marginHorizontal: 0,
    marginVertical: 0
  },
  buttonTxt: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
});
