import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';

export default class Login extends Component {

    constructor(props, context) {
        super(props, context);
        //this.state = { email: "morgan@jennevret.se" };
        this.state = {email: ""};
    }

    render() {
        return (
          <Image source={require('./img/bg.png')} style={styles.background}>
            <Image source={require('./img/balloon.png')} style={styles.background}>
              <TextInput style={styles.email}
                onChangeText={(email) => this.setState({email})}
                autoFocus={true}
                value={this.state.email}
                keyboardType={'email-address'}
                placeholder={'Email:'}
              />
              <TouchableOpacity onPress={this.submitLogin.bind(this)} style={styles.button}>
                <Text style={styles.welcome}>LOG IN</Text>
              </TouchableOpacity>
            </Image>
          </Image>
        );
    }

    submitLogin() {
      const endpointUrl = this.context.apiBase + '/users?email=' + this.state.email;
      fetch(endpointUrl, {method: 'POST'})
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.email) {
          console.log(responseData);
          this.setState({email: responseData.email})
          this.context.setUser({
            email: this.state.email,
          });
        } else {
          alert('Sorry, I can not let you in.')
        }
      });
    }
}

Login.contextTypes = {
  apiBase: React.PropTypes.string,
  getUser: React.PropTypes.any, // Should of course be function, but can't get the validator to accept.
  setUser: React.PropTypes.any // Should of course be function, but can't get the validator to accept.
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    width: null,
    height: null
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  email: {
    color: '#000000',
    fontSize: 20,
    borderColor: '#FFFFFF',
    borderWidth: 1,
    marginHorizontal: 30,
    marginVertical: 5
  },
  button: {
    backgroundColor: '#FFEBCC',
    position: 'absolute',
    bottom: 0,
    height: 50,
    width: Dimensions.get('window').width,
    marginHorizontal: 0,
    marginVertical: 0
  }
});
