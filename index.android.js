import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Login from './components/login.js';
import Send from './components/send.js';

class IOU extends Component {

    constructor(props, context) {
      super(props, context);
      this.state = {currentUser: null};
      this.getChildContext = this.getChildContext.bind(this);
    }

    getChildContext() {
      return {
        apiBase: 'http://cloud.lukaszbrzyski.com:4567',
        getUser: function() {return this.state.currentUser}.bind(this),
        setUser: function(user) {this.setState({currentUser: user})}.bind(this)
      }
    }

    render() {
        return this.currentView.bind(this)();
    }

    currentView() {
        const userLoggedIn = this.state.currentUser;
        if(userLoggedIn) {
          return (
            <Send />
          )
        }
        else {
          return (
            <Login />
          )
        }
    }
}

IOU.childContextTypes = {
  apiBase: React.PropTypes.string,
  getUser: React.PropTypes.any, // Should of course be function, but can't get the validator to accept.
  setUser: React.PropTypes.any // Should of course be function, but can't get the validator to accept.
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('IOU', () => IOU);
